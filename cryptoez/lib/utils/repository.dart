import 'package:cryptoez/models/fetch_coin_model/big_data_model.dart';
import 'package:dio/dio.dart';

class Repository {
  static String mainUrl = "https://pro-api.coinmarketcap.com/v1";
  final String apiKey = "25cacdbf-9b2e-4ecf-8566-3615d2225e15";
  var currencyListStringAPI = "$mainUrl/cryptocurrency/listings/latest";
  var trendingLatestCurrencyListAPI = "$mainUrl/cryptocurrency/trending/latest";

  final Dio _dio = Dio();
  Future<BigDataModel> getCoins() async {
    try {
      _dio.options.headers["X-CMC_PRO_API_KEY"] = apiKey;
      Response response = await _dio.get(currencyListStringAPI);
      return BigDataModel.fromJson(response.data);
    } catch (error, stackTrace) {
      print("exception $error, dd $stackTrace");
      return BigDataModel.withError("error");
    }
  }
}
