import 'package:cached_network_image/cached_network_image.dart';
import 'package:cryptoez/models/ChartDataModel.dart';
import 'package:cryptoez/models/fetch_coin_model/data_model.dart';
import 'package:cryptoez/ui/screens/coin_detail_screen.dart';
import 'package:cryptoez/widgets/coin_logo_widget.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class TopCoinListWidget extends StatelessWidget {
  final List<DataModel> coins;
  final bool? isTopCoin;
  const TopCoinListWidget({
    Key? key,
    required this.coins,
    this.isTopCoin = false,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: _detailTopCoinListBuilder(context),
    );
  }

  Widget _detailTopCoinListBuilder(BuildContext context) {
    return Container(
      margin: const EdgeInsets.symmetric(
        horizontal: 8.0,
      ),
      width: MediaQuery.of(context).size.width,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: _detailTopCoinListContent(context),
      ),
    );
  }

  List<Widget> _detailTopCoinListContent(BuildContext context) {
    List<Widget> items = [];
    int i = 0;
    var coinIconUrl =
        "https://raw.githubusercontent.com/spothq/cryptocurrency-icons/master/128/color/";
    TextTheme textStyle = Theme.of(context).textTheme;

    if (coins.isNotEmpty) {
      while (i < 3) {
        var coin = coins[i];
        items.add(
          GestureDetector(
            onTap: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => CoinDetailScreen(coin: coin)),
              );
            },
            child: Container(
              height: 160.0,
              width: MediaQuery.of(context).size.width / 3.6,
              padding: const EdgeInsets.symmetric(vertical: 4.0),
              margin:
                  const EdgeInsets.symmetric(vertical: 8.0, horizontal: 8.0),
              decoration: BoxDecoration(
                border: Border.all(color: Colors.grey),
                borderRadius: BorderRadius.circular(8.0),
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Container(
                      padding: const EdgeInsets.symmetric(horizontal: 8.0),
                      height: 96.0,
                      width: 96.0,
                      child: CachedNetworkImage(
                        imageUrl: ((coinIconUrl + coin.symbol + ".png")
                            .toLowerCase()),
                        placeholder: (context, url) =>
                            const CircularProgressIndicator(),
                        errorWidget: (context, url, error) =>
                            SvgPicture.asset("assets/icons/dollar.svg"),
                      )),
                  const SizedBox(height: 2.0),
                  Text(
                    coin.name,
                    style: textStyle.subtitle1,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        'Price : ',
                        style: textStyle.caption,
                      ),
                      Text(
                        coin.quoteModel.usdModel.price.toStringAsFixed(0),
                        style: textStyle.caption!.copyWith(color: Colors.blue),
                      ),
                    ],
                  )
                ],
              ),
            ),
          ),
        );
        i++;
      }
    } else {
      items.add(
        const Center(
          child: Text('No data'),
        ),
      );
    }
    return items;
  }
}
