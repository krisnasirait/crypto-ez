import 'package:cached_network_image/cached_network_image.dart';
import 'package:cryptoez/models/ChartDataModel.dart';
import 'package:cryptoez/models/fetch_coin_model/data_model.dart';
import 'package:cryptoez/ui/screens/coin_detail_screen.dart';
import 'package:cryptoez/widgets/coin_logo_widget.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class CoinListWidget extends StatelessWidget {
  final List<DataModel> coins;
  final bool? showAllCoins;
  const CoinListWidget({
    Key? key,
    required this.coins,
    this.showAllCoins = false,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: _detailJournalBalanceBuilder(context),
    );
  }

  Widget _detailJournalBalanceBuilder(BuildContext context) {
    return Column(
      children: _detailJournalBalanceListBuilderLastPage(context),
    );
  }

  List<Widget> _detailJournalBalanceListBuilderLastPage(BuildContext context) {
    List<Widget> items = [];
    int i = 0;
    var coinIconUrl =
        "https://raw.githubusercontent.com/spothq/cryptocurrency-icons/master/128/color/";
    TextTheme textStyle = Theme.of(context).textTheme;

    if (coins.isNotEmpty) {
      int coinsLength = showAllCoins == true ? coins.length : 4;
      while (i < coinsLength) {
        var coin = coins[i];
        var coinPrice = coin.quoteModel.usdModel;
        items.add(GestureDetector(
          onTap: () {
            Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (context) => CoinDetailScreen(coin: coin)),
            );
          },
          child: Container(
            height: 96.0,
            width: double.infinity,
            padding: const EdgeInsets.symmetric(vertical: 4.0),
            margin: const EdgeInsets.symmetric(vertical: 8.0, horizontal: 16.0),
            decoration: BoxDecoration(
              border: Border.all(color: Colors.grey),
              borderRadius: BorderRadius.circular(16.0),
            ),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Expanded(
                  child: Container(
                    padding: const EdgeInsets.only(left: 16.0),
                    height: 96.0,
                    width: 96.0,
                    child: CachedNetworkImage(
                      imageUrl:
                          ((coinIconUrl + coin.symbol + ".png").toLowerCase()),
                      placeholder: (context, url) =>
                          const CircularProgressIndicator(),
                      errorWidget: (context, url, error) =>
                          SvgPicture.asset('assets/icons/dollar.svg'),
                    ),
                  ),
                ),
                Expanded(
                  flex: 2,
                  child: Container(
                    padding: const EdgeInsets.only(left: 16.0),
                    height: 48,
                    alignment: Alignment.centerLeft,
                    width: MediaQuery.of(context).size.width,
                    child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            coin.symbol + ' - USDT',
                            style: textStyle.headline6!.copyWith(
                              color: Colors.blue,
                            ),
                            textAlign: TextAlign.left,
                          ),
                          const SizedBox(height: 8.0),
                          Text.rich(
                            TextSpan(
                              text: 'Price : ',
                              style: Theme.of(context).textTheme.caption,
                              children: <InlineSpan>[
                                TextSpan(
                                  text: coin.quoteModel.usdModel.price
                                      .toStringAsFixed(2),
                                  style: Theme.of(context)
                                      .textTheme
                                      .bodyText1!
                                      .copyWith(
                                        color: Colors.blue,
                                      ),
                                )
                              ],
                            ),
                            textAlign: TextAlign.left,
                          ),
                        ]),
                  ),
                ),
                const SizedBox(height: 4.0),
                Expanded(
                  child: Container(
                    padding: const EdgeInsets.all(4.0),
                    margin: const EdgeInsets.only(right: 16.0),
                    alignment: Alignment.center,
                    width: 72,
                    height: 48,
                    child: Column(children: [
                      Text(
                        "24h chg%",
                        style: textStyle.caption,
                      ),
                      const SizedBox(height: 2.0),
                      Text(
                        coinPrice.percentChange_24h.toStringAsFixed(2) + "%",
                        style: TextStyle(
                            color: coinPrice.percentChange_7d >= 0
                                ? Colors.blue
                                : Colors.red,
                            fontWeight: FontWeight.bold,
                            fontSize: 18.0),
                      ),
                    ]),
                  ),
                ),
              ],
            ),
          ),
        ));
        i++;
      }
    } else {
      items.add(
        Center(
          child: Text('No data'),
        ),
      );
    }
    return items;
  }
}
