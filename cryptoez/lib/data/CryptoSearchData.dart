import 'package:cryptoez/models/CryptoSearchModel.dart';

final allCryptoSearch = <CryptoSearchData>[
  CryptoSearchData(
    id: 0,
    name: 'Bitcoin',
    symbol: 'BTC',
  ),
  CryptoSearchData(
    id: 1,
    name: 'Ethereum',
    symbol: 'ETH',
  ),
  CryptoSearchData(
    id: 2,
    name: 'Solana',
    symbol: 'SOL',
  ),
  CryptoSearchData(
    id: 3,
    name: 'Shiba Inu',
    symbol: 'SHIB',
  ),
  CryptoSearchData(
    id: 4,
    name: 'Dogecoin',
    symbol: 'DOGE',
  ),
  CryptoSearchData(
    id: 5,
    name: 'Binance Coin',
    symbol: 'BNB',
  ),
  CryptoSearchData(
    id: 6,
    name: 'Cardano',
    symbol: 'ADA',
  ),
  CryptoSearchData(
    id: 7,
    name: 'Ripple Token',
    symbol: 'XRP',
  ),
  CryptoSearchData(
    id: 8,
    name: 'Avalanche',
    symbol: 'AVAX',
  ),
  CryptoSearchData(
    id: 9,
    name: 'Terra',
    symbol: 'LUNA',
  ),
  CryptoSearchData(
    id: 10,
    name: 'Litecoin',
    symbol: 'LTC',
  ),
];
