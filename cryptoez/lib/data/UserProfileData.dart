import 'package:cryptoez/models/UserProfileModel.dart';

final allUserProfile = <UserProfileData>[
  UserProfileData(
    email: "carmen@gmail.com",
    name: "carmen",
    kycStatus: "VERIFIED",
    equity: '34.002.321',
    urlProfile:
        'https://w7.pngwing.com/pngs/696/57/png-transparent-mackenzie-bezos-jeff-bezos-amazon-com-the-world-s-billionaires-novelist-efren-ramirez.png',
  ),
  UserProfileData(
    email: "krisna@gmail.com",
    name: "krisna",
    kycStatus: "VERIFIED",
    equity: '391.312.442',
    urlProfile:
        'https://ih1.redbubble.net/image.2255105880.2024/st,small,507x507-pad,600x600,f8f8f8.jpg',
  ),
  UserProfileData(
    email: "sanber@gmail.com",
    name: "sanber code",
    kycStatus: "VERIFIED",
    equity: '23.123.423',
    urlProfile:
        'https://w7.pngwing.com/pngs/896/261/png-transparent-elon-musk-tesla-motors-investor-the-boring-company-spacex-others-face-head-engineer.png',
  ),
];
