import 'package:cached_network_image/cached_network_image.dart';
import 'package:cryptoez/models/fetch_coin_model/data_model.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class CoinDetailScreen extends StatelessWidget {
  final DataModel coin;
  const CoinDetailScreen({
    Key? key,
    required this.coin,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var coinIconUrl =
        "https://raw.githubusercontent.com/spothq/cryptocurrency-icons/master/128/color/";
    final coinUSD = coin.quoteModel.usdModel;

    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          icon: const Icon(Icons.arrow_back, color: Colors.blue),
          onPressed: () => Navigator.of(context).pop(),
        ),
        backgroundColor: Colors.white,
      ),
      body: CustomScrollView(
        slivers: [
          SliverToBoxAdapter(
            child: Column(
              children: [
                Container(
                  width: MediaQuery.of(context).size.width,
                  height: 52.0,
                  margin: const EdgeInsets.fromLTRB(16.0, 32.0, 16.0, 16.0),
                  alignment: Alignment.center,
                  child: Text(
                    coin.symbol + ' - USDT',
                    style: Theme.of(context).textTheme.headline4!.copyWith(
                          color: Colors.blue,
                          fontWeight: FontWeight.bold,
                        ),
                  ),
                ),
                Stack(
                  children: [
                    Container(
                      padding:
                          const EdgeInsets.fromLTRB(16.0, 72.0, 16.0, 24.0),
                      margin: const EdgeInsets.fromLTRB(16.0, 48.0, 16.0, 16.0),
                      decoration: BoxDecoration(
                          border: Border.all(color: Colors.blue),
                          borderRadius: BorderRadius.circular(8.0)),
                      width: MediaQuery.of(context).size.width,
                      height: MediaQuery.of(context).size.height,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          getRowText(
                            context,
                            labelTitle: 'Price',
                            textValue: coinUSD.price.toStringAsFixed(0),
                            textValueStyle:
                                Theme.of(context).textTheme.headline4!.copyWith(
                                      color: Colors.blue,
                                    ),
                          ),
                          getRowText(
                            context,
                            labelTitle: 'Volume 24h',
                            textValue: coinUSD.volume24h.toStringAsFixed(0),
                          ),
                          getRowText(
                            context,
                            labelTitle: 'Change 24h',
                            textValue:
                                coinUSD.percentChange_24h.toStringAsFixed(2) +
                                    '%',
                          ),
                          getRowText(context, labelTitle: 'Token details'),
                          Padding(
                            padding: const EdgeInsets.symmetric(
                              vertical: 10.0,
                              horizontal: 16.0,
                            ),
                            child: Text(
                              getCoinDetails(coin.tags),
                              overflow: TextOverflow.ellipsis,
                              maxLines: 20,
                              style: Theme.of(context).textTheme.bodyText2,
                            ),
                          ),
                        ],
                      ),
                    ),
                    Container(
                      width: MediaQuery.of(context).size.width,
                      height: 96.0,
                      alignment: Alignment.center,
                      child: CachedNetworkImage(
                        width: 96.0,
                        height: 96.0,
                        imageUrl: ((coinIconUrl + coin.symbol + ".png")
                            .toLowerCase()),
                        placeholder: (context, url) =>
                            const CircularProgressIndicator(),
                        errorWidget: (context, url, error) =>
                            SvgPicture.asset('assets/icons/dollar.svg'),
                      ),
                    ),
                  ],
                )
              ],
            ),
          ),
        ],
      ),
    );
  }

  Container getRowText(
    BuildContext context, {
    String? labelTitle,
    String? textValue,
    TextStyle? textValueStyle,
  }) =>
      Container(
        alignment: Alignment.centerLeft,
        padding: const EdgeInsets.symmetric(vertical: 10.0),
        child: Text.rich(
          TextSpan(
            text: labelTitle != null ? '$labelTitle : ' : 'Price : ',
            style: Theme.of(context).textTheme.headline6!.copyWith(
                  color: Colors.grey,
                ),
            children: <InlineSpan>[
              TextSpan(
                text: textValue ?? '',
                style: textValueStyle ??
                    Theme.of(context).textTheme.headline5!.copyWith(
                          color: Colors.blue,
                        ),
              )
            ],
          ),
          textAlign: TextAlign.left,
        ),
      );

  String getCoinDetails(List<dynamic> tags) {
    String details = '';
    if (tags.isNotEmpty) {
      details = tags.join("\n");
    }
    return details;
  }
}
