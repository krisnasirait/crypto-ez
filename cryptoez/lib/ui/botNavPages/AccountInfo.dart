import 'package:cryptoez/ui/LoginScreen.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:cryptoez/models/fetch_coin_model/big_data_model.dart';
import 'package:cryptoez/models/fetch_coin_model/data_model.dart';
import 'package:cryptoez/utils/repository.dart';
import 'package:cryptoez/widgets/coin_list_widget.dart';
import 'package:cryptoez/widgets/top_coin_list_widget.dart';

class AccountInfo extends StatefulWidget {
  AccountInfo({Key? key}) : super(key: key);

  @override
  _AccountInfoState createState() => _AccountInfoState();
}

class _AccountInfoState extends State<AccountInfo> {
  final _auth = FirebaseAuth.instance;
  late Future<BigDataModel> _futureCoins;
  late Repository repository;
  late bool showAllCoins;

  dynamic user;

  late String userEmail;

  void getEmailData() {
    userEmail = _auth.currentUser!.email!;
    userEmail.toString();
  }

  @override
  void initState() {
    repository = Repository();
    _futureCoins = repository.getCoins();
    showAllCoins = false;

    getEmailData();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    var container = new Container(
      child: Text("Data"),
      padding: EdgeInsets.only(left: 5.0, right: 5.0, top: 5.0, bottom: 5.0),
      width: 200.0,
      height: 200.0,
    );

    return SafeArea(
        child: Scaffold(
            appBar: AppBar(
              actions: [
                PopupMenuButton<int>(
                    onSelected: (item) => onSelected(context, item),
                    itemBuilder: (context) => [
                          PopupMenuItem<int>(
                            value: 0,
                            child: Text("Sign Out"),
                          ),
                        ]),
              ],
              title: Text("Crypto EZ"),
            ),
            body: _getData()));
  }

  void onSelected(BuildContext context, int item) {
    switch (item) {
      case 0:
        Future<void> signOut() async {
          await FirebaseAuth.instance.signOut();
        }
        Navigator.pushReplacement(
            context, MaterialPageRoute(builder: (context) => LoginScreen()));
        break;
    }
  }

  Widget _getData() => FutureBuilder<BigDataModel>(
      future: _futureCoins,
      builder: (context, snapshot) {
        if (snapshot.connectionState == ConnectionState.done) {
          if (snapshot.hasData) {
            return SingleChildScrollView(
              child: Column(children: [
                getLabelName(label: "Top Coin"),
                TopCoinListWidget(coins: snapshot.data!.dataModel),
                Row(
                  children: [
                    Expanded(
                      flex: 1,
                      child: getLabelName(label: "All Coin"),
                    ),
                    Expanded(
                      flex: 1,
                      child: InkWell(
                        onTap: () {
                          setState(() {
                            if (showAllCoins == false) {
                              showAllCoins = true;
                            } else if (showAllCoins == true) {
                              showAllCoins = false;
                            }
                          });
                        },
                        child: getLabelName(
                          label: showAllCoins == false ? "See All" : "Hide",
                          textStyle: const TextStyle(
                            color: Colors.blue,
                            fontSize: 22.0,
                          ),
                          textAlign: TextAlign.right,
                        ),
                      ),
                    ),
                  ],
                ),
                CoinListWidget(
                    coins: snapshot.data!.dataModel,
                    showAllCoins: showAllCoins),
              ]),
            );
          } else if (snapshot.hasError) {
            return Center(
              child: Text('${snapshot.error}'),
            );
          }
        }
        return const Center(child: CircularProgressIndicator());
      });

  Container getLabelName({
    String? label,
    TextStyle? textStyle,
    TextAlign? textAlign,
  }) =>
      Container(
        padding: const EdgeInsets.fromLTRB(16.0, 8.0, 16.0, 8.0),
        width: MediaQuery.of(context).size.width,
        child: Text(label ?? '-',
            style: textStyle ??
                const TextStyle(
                  color: Colors.black,
                  fontSize: 24.0,
                ),
            textAlign: textAlign ?? TextAlign.left),
      );
}
