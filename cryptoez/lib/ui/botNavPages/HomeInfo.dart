import 'package:cryptoez/data/UserProfileData.dart';
import 'package:cryptoez/models/UserProfileModel.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';

class HomeInfo extends StatefulWidget {
  HomeInfo({Key? key}) : super(key: key);

  @override
  _HomeInfoState createState() => _HomeInfoState();
}

class _HomeInfoState extends State<HomeInfo> {
  late List<UserProfileData> userProfileData;

  final _auth = FirebaseAuth.instance;
  dynamic user;

  late String userEmail;
  late String username;
  late String equity;
  late String kycStatus;
  late String urlProfile;

  void getEmailData() {
    userEmail = _auth.currentUser!.email!;
    userEmail.toString();
  }

  @override
  void initState() {
    getEmailData();
    userProfileData = allUserProfile;
    getUserProfileData(userEmail);
    super.initState();
  }

  void getUserProfileData(String useremail) {
    int length = userProfileData.length;

    for (int i = 0; i < length; i++) {
      if (userEmail == userProfileData[i].email.toString()) {
        username = userProfileData[i].name.toString();
        equity = userProfileData[i].equity.toString();
        kycStatus = userProfileData[i].kycStatus.toString();
        urlProfile = userProfileData[i].urlProfile.toString();
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Scaffold(
      backgroundColor: Color(0xFF4091FF),
      body: Column(
        children: <Widget>[
          Container(
            width: 412,
            height: 210,
            decoration: BoxDecoration(
              image: DecorationImage(
                  image: NetworkImage(
                      "https://cdn.wallpapersafari.com/76/93/URX4o9.jpg"),
                  fit: BoxFit.cover),
            ),
            child: Padding(
              padding: const EdgeInsets.only(left: 25),
              child: Row(
                children: [
                  CircleAvatar(
                    maxRadius: 60,
                    minRadius: 60,
                    backgroundImage: NetworkImage(urlProfile),
                    backgroundColor: Colors.transparent,
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 31, top: 79),
                    child: Column(
                      children: [
                        Text(
                          username,
                          style: TextStyle(
                              fontFamily: 'Roboto',
                              fontWeight: FontWeight.w700,
                              fontSize: 20,
                              color: Colors.white),
                        ),
                        Padding(
                          padding: EdgeInsets.only(top: 5),
                        ),
                        Container(
                          width: 104,
                          height: 29,
                          decoration: BoxDecoration(
                            color: Color(0xFF4EC2B1),
                            borderRadius: BorderRadius.all(
                              Radius.circular(10),
                            ),
                          ),
                          child: Center(
                            child: Text(
                              kycStatus,
                              style: TextStyle(
                                  fontSize: 14,
                                  fontWeight: FontWeight.w500,
                                  color: Colors.white),
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(
              top: 14,
            ),
            child: Container(
              width: 360,
              height: 145,
              decoration: BoxDecoration(
                color: Color(0xFFFFC250),
                borderRadius: BorderRadius.all(
                  Radius.circular(10),
                ),
              ),
              child: Column(
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.only(top: 15, left: 15),
                    child: Text(
                      "Equity Value (Rp)",
                      style: TextStyle(
                          fontSize: 28,
                          fontFamily: 'Roboto',
                          fontWeight: FontWeight.w700,
                          color: Colors.white),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 40, left: 15),
                    child: Text(
                      equity,
                      style: TextStyle(
                          fontSize: 24,
                          fontFamily: 'Roboto',
                          fontWeight: FontWeight.w500,
                          color: Colors.black),
                    ),
                  )
                ],
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(left: 25, top: 8),
            child: Text(
              "All Assets : ",
              style: TextStyle(
                  fontWeight: FontWeight.w700,
                  fontSize: 28,
                  fontFamily: 'Roboto',
                  color: Colors.white),
            ),
          ),
          Expanded(
              child: SingleChildScrollView(
            child: Container(
              constraints: BoxConstraints(
                maxHeight: double.infinity,
              ),
              child: Column(
                children: <Widget>[
                  Card(
                    color: Color(0xFFFFC250),
                    child: ListTile(
                      title: Text("Bitcoin"),
                      subtitle: Text("BTC"),
                      trailing: Text("0.5823900123"),
                    ),
                  ),
                  Card(
                    color: Color(0xFFFFC250),
                    child: ListTile(
                      title: Text("Binance Coin"),
                      subtitle: Text("BNB"),
                      trailing: Text("6,72"),
                    ),
                  ),
                  Card(
                    color: Color(0xFFFFC250),
                    child: ListTile(
                      title: Text("Solana"),
                      subtitle: Text("SOL"),
                      trailing: Text("312.239"),
                    ),
                  ),
                  Card(
                    color: Color(0xFFFFC250),
                    child: ListTile(
                      title: Text("Shiba Inu"),
                      subtitle: Text("SHIB"),
                      trailing: Text("320.210.445"),
                    ),
                  ),
                ],
              ),
            ),
          ))
        ],
      ),
    ));
  }
}
