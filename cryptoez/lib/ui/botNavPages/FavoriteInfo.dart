import 'package:flutter/material.dart';

class FavoriteInfo extends StatefulWidget {
  FavoriteInfo({Key? key}) : super(key: key);

  @override
  _FavoriteInfoState createState() => _FavoriteInfoState();
}

class _FavoriteInfoState extends State<FavoriteInfo> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Scaffold(
      body: Column(
        children: <Widget>[
          Expanded(
              child: SingleChildScrollView(
            child: Container(
              constraints: BoxConstraints(
                maxHeight: double.infinity,
              ),
              child: Column(
                children: <Widget>[
                  Padding(
                    padding: EdgeInsets.only(top: 20),
                  ),
                  Text(
                    "Favorites Tokens :",
                    style: TextStyle(
                      fontFamily: 'Roboto',
                      fontWeight: FontWeight.w700,
                      color: Colors.blue,
                      fontSize: 25,
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.only(top: 20),
                  ),
                  Card(
                    color: Color(0xFF42C6FF),
                    child: ListTile(
                      title: Text("Bitcoin"),
                      subtitle: Text("BTC"),
                      trailing: Icon(
                        Icons.favorite,
                        color: Colors.red,
                      ),
                    ),
                  ),
                  Card(
                    color: Color(0xFF42C6FF),
                    child: ListTile(
                      title: Text("Binance Coin"),
                      subtitle: Text("BNB"),
                      trailing: Icon(
                        Icons.favorite,
                        color: Colors.red,
                      ),
                    ),
                  ),
                  Card(
                    color: Color(0xFF42C6FF),
                    child: ListTile(
                      title: Text("Solana"),
                      subtitle: Text("SOL"),
                      trailing: Icon(
                        Icons.favorite,
                        color: Colors.red,
                      ),
                    ),
                  ),
                  Card(
                    color: Color(0xFF42C6FF),
                    child: ListTile(
                      title: Text("Shiba Inu"),
                      subtitle: Text("SHIB"),
                      trailing: Icon(
                        Icons.favorite,
                        color: Colors.red,
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ))
        ],
      ),
    ));
  }
}
