import 'dart:ffi';

import 'package:cryptoez/data/CryptoSearchData.dart';
import 'package:cryptoez/models/CryptoSearchModel.dart';
import 'package:cryptoez/widgets/search_widget.dart';
import 'package:flutter/material.dart';
import 'package:material_floating_search_bar/material_floating_search_bar.dart';

class SearchInfo extends StatefulWidget {
  SearchInfo({Key? key}) : super(key: key);

  @override
  _SearchInfoState createState() => _SearchInfoState();
}

class _SearchInfoState extends State<SearchInfo> {
  late List<CryptoSearchData> cryptoSearchData;

  String query = '';

  @override
  void initState() {
    super.initState();
    cryptoSearchData = allCryptoSearch;
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: Column(
          children: <Widget>[
            SearchWidget(
              text: query,
              hintText: 'Search Token Here',
              onChanged: searchCrypto,
            ),
            Expanded(
                child: ListView.builder(
              itemCount: cryptoSearchData.length,
              itemBuilder: (context, index) {
                final cryptoSearch = cryptoSearchData[index];
                return buildCryptoSearch(cryptoSearch);
              },
            ))
          ],
        ),
      ),
    );
  }

  Widget buildCryptoSearch(CryptoSearchData cryptoSearchData) => ListTile(
        title: Text(cryptoSearchData.name),
        subtitle: Text(cryptoSearchData.symbol),
      );

  void searchCrypto(String query) {
    final cryptoSearchData = allCryptoSearch.where((cryptoSearchData) {
      final nameLower = cryptoSearchData.name.toString().toLowerCase();
      final symbolLower = cryptoSearchData.symbol.toString().toLowerCase();
      final searchLower = query.toString().toLowerCase();

      return nameLower.contains(searchLower) ||
          symbolLower.contains(searchLower);
    }).toList();

    setState(() {
      this.query = query;
      this.cryptoSearchData = cryptoSearchData;
    });
  }
}
