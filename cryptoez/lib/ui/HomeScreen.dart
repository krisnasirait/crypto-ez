import 'dart:convert';

import 'package:cryptoez/ui/LoginScreen.dart';
import 'package:cryptoez/ui/botNavPages/AccountInfo.dart';
import 'package:cryptoez/ui/botNavPages/FavoriteInfo.dart';
import 'package:cryptoez/ui/botNavPages/HomeInfo.dart';
import 'package:cryptoez/ui/botNavPages/SearchInfo.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

class HomeScreen extends StatefulWidget {
  HomeScreen({Key? key}) : super(key: key);

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  @override
  void initState() {
    super.initState();
  }

  int _selectedIndex = 0;

  void _onItemTap(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  final screens = [
    AccountInfo(),
    SearchInfo(),
    FavoriteInfo(),
    HomeInfo(),
  ];

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        bottomNavigationBar: BottomNavigationBar(
          type: BottomNavigationBarType.fixed,
          backgroundColor: Color(0xFF88BBFF),
          unselectedItemColor: Colors.white,
          items: <BottomNavigationBarItem>[
            BottomNavigationBarItem(
              icon: Icon(Icons.home),
              label: 'Home',
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.search),
              label: 'Search',
              backgroundColor: Color(0xFF88BBFF),
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.star),
              label: 'Favorite',
              backgroundColor: Color(0xFF88BBFF),
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.account_box),
              label: 'Account',
              backgroundColor: Color(0xFF88BBFF),
            ),
          ],
          currentIndex: _selectedIndex,
          selectedItemColor: Colors.blue,
          onTap: _onItemTap,
        ),
        body: screens[_selectedIndex],
      ),
    );
  }
}
