// To parse this JSON data, do
//
//     final cryptoModels = cryptoModelsFromJson(jsonString);

import 'package:meta/meta.dart';
import 'dart:convert';

CryptoModels cryptoModelsFromJson(String str) =>
    CryptoModels.fromJson(json.decode(str));

String cryptoModelsToJson(CryptoModels data) => json.encode(data.toJson());

class CryptoModels {
  CryptoModels({
    required this.exchangeId,
    required this.symbol,
    required this.baseAsset,
    required this.quoteAsset,
    required this.priceUnconverted,
    required this.price,
    required this.change24H,
    required this.spread,
    required this.volume24H,
    required this.status,
    required this.createdAt,
    required this.updatedAt,
  });

  String exchangeId;
  String symbol;
  String baseAsset;
  String quoteAsset;
  double priceUnconverted;
  double price;
  double change24H;
  double spread;
  double volume24H;
  String status;
  DateTime createdAt;
  DateTime updatedAt;

  factory CryptoModels.fromJson(Map<String, dynamic> json) => CryptoModels(
        exchangeId: json["exchange_id"],
        symbol: json["symbol"],
        baseAsset: json["base_asset"],
        quoteAsset: json["quote_asset"],
        priceUnconverted: json["price_unconverted"].toDouble(),
        price: json["price"].toDouble(),
        change24H: json["change_24h"].toDouble(),
        spread: json["spread"].toDouble(),
        volume24H: json["volume_24h"].toDouble(),
        status: json["status"],
        createdAt: DateTime.parse(json["created_at"]),
        updatedAt: DateTime.parse(json["updated_at"]),
      );

  Map<String, dynamic> toJson() => {
        "exchange_id": exchangeId,
        "symbol": symbol,
        "base_asset": baseAsset,
        "quote_asset": quoteAsset,
        "price_unconverted": priceUnconverted,
        "price": price,
        "change_24h": change24H,
        "spread": spread,
        "volume_24h": volume24H,
        "status": status,
        "created_at": createdAt.toIso8601String(),
        "updated_at": updatedAt.toIso8601String(),
      };
}
