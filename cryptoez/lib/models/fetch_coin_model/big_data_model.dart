import 'package:cryptoez/models/fetch_coin_model/data_model.dart';
import 'package:cryptoez/models/fetch_coin_model/status_model.dart';

class BigDataModel {
  final StatusModel statusModel;
  final List<DataModel> dataModel;

  BigDataModel({
    required this.statusModel,
    required this.dataModel,
  });

  factory BigDataModel.fromJson(Map<String, dynamic> json) {
    var dataList = json["data"] as List;
    List<DataModel> dataModelList =
        dataList.map((e) => DataModel.fromJson(e)).toList();
    return BigDataModel(
      statusModel: StatusModel.fromJson(json["status"]),
      dataModel: dataModelList,
    );
  }
  BigDataModel.withError(String error)
      : statusModel = StatusModel(
          timeStamp: error,
          errorCode: 0,
          errorMessage: error,
          elapsed: 0,
          creditCount: 0,
          notice: error,
          totalCount: 0,
        ),
        dataModel = [];
}
