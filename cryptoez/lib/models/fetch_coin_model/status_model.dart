class StatusModel {
  final String timeStamp;
  final int errorCode;
  final String errorMessage;
  final int elapsed;
  final int creditCount;
  final String notice;
  final int totalCount;

  StatusModel({
    required this.timeStamp,
    required this.errorCode,
    required this.errorMessage,
    required this.elapsed,
    required this.creditCount,
    required this.notice,
    required this.totalCount,
  });
  factory StatusModel.fromJson(Map<String, dynamic> json) {
    return StatusModel(
      timeStamp: json["timestamp"] ?? '',
      errorCode: json["error_code"] ?? 0,
      errorMessage: json["error_message"] ?? '',
      elapsed: json["elapsed"] ?? 0,
      creditCount: json["credit_count"] ?? 0,
      notice: json["notice"] ?? '',
      totalCount: json["total_count"] ?? '',
    );
  }
}
