class CryptoSearchData {
  final int id;
  final String name;
  final String symbol;

  const CryptoSearchData({
    required this.id,
    required this.name,
    required this.symbol,
  });

  factory CryptoSearchData.fromJson(Map<String, dynamic> json) =>
      CryptoSearchData(
        id: json['id'],
        name: json['name'],
        symbol: json['symbol'],
      );

  Map<String, dynamic> toJson() => {
        'id': id,
        'name': name,
        'symbol': symbol,
      };
}
