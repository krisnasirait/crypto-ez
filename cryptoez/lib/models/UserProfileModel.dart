class UserProfileData {
  final String email;
  final String name;
  final String kycStatus;
  final String equity;
  final String urlProfile;

  const UserProfileData({
    required this.email,
    required this.name,
    required this.kycStatus,
    required this.equity,
    required this.urlProfile,
  });

  factory UserProfileData.fromJson(Map<String, dynamic> json) =>
      UserProfileData(
        email: json['email'],
        name: json['name'],
        kycStatus: json['kycStatus'],
        equity: json['equity'],
        urlProfile: json['urlProfile'],
      );

  Map<String, dynamic> toJson() => {
        'email': email,
        'name': name,
        'kycStatus': kycStatus,
        'equity': equity,
        'urlProfile': urlProfile,
      };
}
